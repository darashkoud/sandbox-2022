'use strict';

let header = document.getElementById('main-header');

const links = document.querySelectorAll('[data-router-link]');

Array.from(links).forEach(link => {
    link.addEventListener('click', () => {
        const { routerLink } = link.dataset;
        history.pushState(null, null, routerLink);
        setHeader(routerLink);
    });
})

window.addEventListener('popstate',(event) => {
    setHeader(event.currentTarget.location.pathname);
});

const setHeader = value => header.innerHTML = value;